"CmdLine script to load project"

"Fix-up issue with minimum image missing STON"
{ MCFileTreeRepository. MCFileTreeStCypressReader. MCFileTreeAbstractReader } do: [:c | c compileAll ].

Metacello new
    repository: 'filetree://../src';
    baseline: 'Lambda';
    load.

Metacello new
    repository: 'filetree://../src';
    baseline: 'LambdaRuntime';
    load.

"Stop logging changes and reading source files in read/only environment"
(Smalltalk at: #NoChangesLog) install.
(Smalltalk at: #NoPharoFilesOpener) install.