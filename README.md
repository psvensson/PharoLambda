![Pharo](http://pharo.org/web/files/pharo.png)


# PharoLambda

[![build status](https://gitlab.com/macta/PharoLambda/badges/master/build.svg)](https://gitlab.com/macta/PharoLambda/) 


## Overview

PharoLambda is a simple example and GitLab [build script](./.gitlab-ci.yml) for deploying a minimal 
[Pharo Smalltalk](https://pharo.org) image/vm to [AWS Lambda](https://aws.amazon.com/lambda/).

This work is loosely based on this [tutorial](https://aws.amazon.com/blogs/compute/scripting-languages-for-aws-lambda-running-php-ruby-and-go/) for other languages, 
however it automates the manual steps and provides a suitable NodeJS shim file. 

As noted in the related [Sparta](https://github.com/mweagle/Sparta/blob/master/README.md) GO project, 
there are some known AWS [limitations](http://gosparta.io/docs/limitations/) 
that also apply to Pharo, namely an initialization penalty of around ~700ms (depending on container reuse). 
This said, empirical results seem to show execution times of between 500-1400ms for Pharo.

## Running It

You will need to fork this project on GitLab, setup an AWS Lambda account, and configue [AWS access keys](http://docs.aws.amazon.com/cli/latest/userguide/cli-environment.html)
 in your Gitlab build pipeline secret variables settings, as well as a $LAMBDA_ROLE variable defining the arn for your "lambda_basic_execution" role. Refer to the 
 comment in the variables section of the [build script](./.gitlab-ci.yml).

The build script will create a zip file containing a NodeJS shim [function](./PharoLambda.js) as well as a [Pharo 64bit](http://pharo.org/gnu-linux-installation-64)
linux vm and an image containing the compiled [Metacello](https://github.com/dalehenrich/metacello-work) packages in this repo.

You can also create a simple [Alexa skill](https://developer.amazon.com/edw/home.html#/skills) to call your PharoLambda function by following 
the crux of this [tutorial](https://github.com/Donohue/alexa), and using the included intents and utterences [samples](./alexa). You will need
to add an Alexa Skills Kit trigger to the created lambda function in the AWS console.

## Future Work

This project only scratches the surface of what is possible with Smalltalk and Lambda. There is still more to do.
- create a light weight framework to integrate with other AWS services like DynamoDB etc.
- leverage Smalltalk's amazing image serialization to provide sophisticated post mortem debugging

## Learning More

While Smalltalk is the grandfather of Object Oriented programming languages and live programming, it's
often undiscovered. There are fortunately many great resources for learning this friendly little language:
- [Pharo Overview](https://www.youtube.com/watch?v=xhPlUaXpCU4) with [slides](http://www.slideshare.net/MarcusDenker/pharo-objects-at-your-fingertips)
- [Syntax in minutes](https://learnxinyminutes.com/docs/smalltalk/)
- [Deep Into Pharo](http://deepintopharo.com/)
- [Pharo Open Source Books](https://github.com/SquareBracketAssociates)
- [Pharo Screencasts](http://www.jarober.com/blog/blogView?content=st4u_pharo)
- [Live Coding](https://vimeo.com/97315968) example
- [Early History of Smalltalk](http://worrydream.com/EarlyHistoryOfSmalltalk/) is the amazing story of the language and how it came to be (worth a read)
